package com.edp;

import java.util.ArrayList;
import java.util.List;

import com.padapter.common.declare.PAction;
import com.padapter.common.declare.PApi;
import com.padapter.common.declare.PClassFactory;
import com.padapter.common.declare.PDefine;
import com.padapter.common.declare.PDefine.EVENTTYPE;
import com.padapter.common.declare.PDefine.JOBTYPE;
import com.padapter.common.declare.PThread;
import com.padapter.listener.daemon.OpenAdrAction;
import com.padapter.listener.daemon.SmpAction;
import com.padapter.model.job.JobModel;
import com.padapter.model.plugin.PluginDaemonOAdrModel;
import com.padapter.model.plugin.PluginDaemonSmpModel;

public class Api extends PApi{
	private Task task = new Task(this);

	public Api() {
		pm = PClassFactory.createPluginModel(PDefine.PLUGINTYPE.DAEMON_SMP);
	}

	@Override
	public void setPluginName(String name){
		pm.setName(name);
	}

	@Override
	public void setPluginPath(String path){
		pm.setPath(path);
	}

	@Override
	public void setPluginAction(PAction action){
		this.action = (SmpAction)action;
		updateState(PDefine.PLUGINSTATE.READY);
	}

	@Override
	public PluginDaemonSmpModel getPluginModel() {
		return (PluginDaemonSmpModel)pm;
	}

	@Override
	public SmpAction getPluginAction() {
		return (SmpAction) action;
	}

	@Override
	public boolean execute(JobModel job){
		return task.pThread_Run(job);
	}

	@Override
	public boolean start() {
		List<EVENTTYPE> eventTypeList = new ArrayList<>();
		pThread = PThread.start(this, task, null, eventTypeList);
		updateState(PDefine.PLUGINSTATE.RUNNING);
		return execute(null);
	}

	@Override
	public boolean stop(){
		return true;
	}

	@Override
	public boolean close() {
		return super.clear();
	}
}
