package com.edp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.edp.smp.RunnerSmpCrawler;
import com.padapter.common.declare.PDefine;
import com.padapter.common.declare.PThread;
import com.padapter.common.declare.PThreadAction;
import com.padapter.model.job.JobModel;
import com.padapter.model.plugin.PluginDaemonSmpModel.PluginDaemonSmpParamModel;

public class Task implements PThreadAction {
	private static Logger log = LoggerFactory.getLogger(Task.class);
	private Api api=null;

	public Task(Api api){
		this.api = api;
	}

	@Override
	public void pThread_New(PThread thread) {
		//thread를 생성하지 않는경우 호출되지 않는다.
	}

	@Override
	public boolean pThread_Run(JobModel job) {
		boolean ret = true;
		PluginDaemonSmpParamModel param = api.getPluginModel().getParam();
		RunnerSmpCrawler.getInstance().start(RunnerSmpCrawler.class.getName(), api, param.getStartHHmm(), PDefine.PERIODSEC.ONE_DAY);
		return ret;
	}
	@Override
	public void pThread_Close() {
		RunnerSmpCrawler.getInstance().close();
	}
}
