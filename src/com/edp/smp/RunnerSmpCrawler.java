package com.edp.smp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.edp.Api;
import com.padapter.common.declare.PTimerTask;
import com.padapter.common.declare.PDefine.LANDTYPE;
import com.padapter.common.util.DateUtil;
import com.padapter.common.util.StringUtil;
import com.padapter.external.req.PluginSmpReq;
import com.padapter.model.SmpDayModel;
import com.padapter.model.SmpDayModel.SmpTimeModel;

public class RunnerSmpCrawler extends PTimerTask {
	private static Logger log = LoggerFactory.getLogger(RunnerSmpCrawler.class);
//	private final String URL = "http://onerec.kmos.kr/mobile/rec/selectRecSMPList.do?key=1931";

	/** The instance. */
    private volatile static RunnerSmpCrawler instance;

    /**
  	 * Instantiates a new AmiCBL.
  	 */
	public RunnerSmpCrawler() {
	}

	/**
	 * Gets the single instance of AmiReadingTask.
	 *
	 * @return single instance of AmiReadingTask
	 */
    public static RunnerSmpCrawler getInstance() {
		if(instance == null) {
			synchronized (RunnerSmpCrawler.class) {
				if(instance == null) {
					instance = new RunnerSmpCrawler();
				}
			}
		}
		return instance;
	}
	@Override
	public void run() {
		log.info("======      Smp grab start      ======");
		try {
			Date today = DateUtil.inst().getToday();
			for(LANDTYPE landtype : LANDTYPE.values()) {
				if(landtype == LANDTYPE.UNKNOWN)
					continue;

				Document doc = getDocument(today, landtype);
				if(doc == null) {
					log.warn("document is not existed");
					return;
				}

				PluginSmpReq req = new PluginSmpReq();
				SmpDayModel smp = readElement(doc, today);
				smp.setCdt(today);
				smp.setLandtype(landtype.value);
				req.setSmp(smp);

				Api api = (Api) getApi();
				api.getPluginAction().plugin_DaemonSmp_Add_SmpModel(req);
			}
		} catch (Exception e) {
			log.error("Exception", e);
		}
		log.info("======      Smp grab end      ======");
	}

	private Document getDocument(Date today, LANDTYPE landtype) throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("excelYN", "N");
		map.put("area_type", String.valueOf(KPXLANDTYPE.findLANDTYPE(landtype)));		//1 = 육지, 9 = 제주
		map.put("searchType", "hour");
		map.put("searchDate", DateUtil.inst().formatDate(today, "yyyy-MM-dd"));
		map.put("searchDate_form", "");
		map.put("searchDate_to", "");
		map.put("year_from1","2019");
		map.put("month_from1","01");
		map.put("year_to1", "2019");
		map.put("month_to1","01");
		map.put("year_from2", "2019");
		map.put("year_to2", "2019");

		Api api = (Api) getApi();
		Document document = Jsoup.connect(api.getPluginModel().getParam().getUrl()).data(map).post();
		return document;
	}

	private SmpDayModel readElement(Document doc, Date today) throws Exception {
		SmpDayModel smp = new SmpDayModel();
		Element content = doc.getElementById("div1_t");
		if(content == null)
			throw new Exception("content element is not existed");

		Elements days = content.select("table thead tr th");
		if(days == null)
			throw new Exception("days element is not existed");

		Elements datas = content.select("table tbody tr");
		if(datas == null)
			throw new Exception("datas element is not existed");

		int index = readElementDayIndex(days, today);
		List<SmpTimeModel> times = readElementData(datas, index);
		if(times.isEmpty())
			throw new Exception("SmpTime list is Empty");

		smp.setDay(DateUtil.inst().yyyyMMdd(today));
		smp.getTimes().addAll(times);
		return smp;
	}

	private int readElementDayIndex(Elements days, Date today) throws Exception {
		int index = 0;
		for(Element day : days) {
			if(days.indexOf(day) == 0)
				continue;
			String text = day.text();
			String[] strs = StringUtil.inst().splitStringAtCharacter(text, ' ');
			strs = StringUtil.inst().splitStringAtCharacter(strs[0], '.');
			String d = strs[0]+strs[1];

			if(d.equals(DateUtil.inst().MMdd_string(today))) {
				index = days.indexOf(day);
				break;
			}
		}
		return index;
	}

	private List<SmpTimeModel> readElementData(Elements datas, int index) throws Exception {
		List<SmpTimeModel> res = new ArrayList<>();
		for(Element data : datas) {
			String[] strs = StringUtil.inst().splitStringAtCharacter(data.text(), ' ');
			if(strs.length == 1) {
				continue;
			}
			SmpTimeModel time = new SmpTimeModel();
			for(int i = 0; i < strs.length; ++i) {
				if(strs[i].isEmpty())
					continue;
				if(i == 0) {
					time.setH(Integer.parseInt(strs[i].replaceAll("h", ""))-1);
				}else if(i == index) {
					time.setPrice(Double.parseDouble(strs[i]));
				}
			}
			res.add(time);
		}
		return res;
	}

	private enum KPXLANDTYPE {
		LAND(1),
		ISLAND(9);

		private final int value;

		private KPXLANDTYPE(int value) {
			this.value = value;
		}

		public static int findLANDTYPE(LANDTYPE type) throws Exception {
			switch(type) {
			case LAND:
				return LAND.value;
			case ISLAND:
				return ISLAND.value;
			case UNKNOWN:
				throw new Exception("invalid landtype : "+type.toString());
			}
			return 0;
		}
	}
}
